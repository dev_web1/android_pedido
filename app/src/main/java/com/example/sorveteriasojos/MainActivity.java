package com.example.sorveteriasojos;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    Spinner spinnerTipo;
    List<String> listaTipo;
    Button btnfinalizar;
    EditText edtQuantidade;
    RadioGroup rgSabor;
    //RadioButton btRadio;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        spinnerTipo =findViewById(R.id.spTipo);
        btnfinalizar = findViewById(R.id.btnFinalizar);
        edtQuantidade = findViewById(R.id.edtQuantidade);
        rgSabor = findViewById(R.id.rgSabor);
        carregarListaTipo();
    }
    public void carregarListaTipo(){
        listaTipo = new ArrayList<>();
        listaTipo.add("Cone");
        listaTipo.add("Sunday");
        listaTipo.add("Picolé");
        listaTipo.add("Bola Dupla");

        ArrayAdapter adp = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, listaTipo);
        adp.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerTipo.setAdapter(adp);

        spinnerTipo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                String selected = listaTipo.get(position);
                //teste
                Toast.makeText(getApplicationContext(), selected, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }
    public void onClick(View v){
        //String texto;
        if (v.getId() == R.id.btnFinalizar){
            //retorna o item selecionado pelo usuario no spinner
           // texto = spinnerTipo.getSelectedItem().toString();
            AlertDialog.Builder dlg = new AlertDialog.Builder(this);
            dlg.setTitle(R.string.app_name);
            dlg.setMessage("Você tem certeza que deseja finalizar o pedido?");
            dlg.setNegativeButton("Sim", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i)
                {
                    abrir_atividade();
                }
            });
            dlg.setPositiveButton("Não", null);
            dlg.show();
        }
    }

    public void abrir_atividade(){
        //definir parametros
        String tipo = spinnerTipo.getSelectedItem().toString();
        int saborId = rgSabor.getCheckedRadioButtonId();
        RadioButton rdb = findViewById(saborId);
        String sabor = rdb.getText().toString();

        int qtde = 0;
        if (edtQuantidade.getText().length() > 0) {
            qtde = Integer.parseInt(edtQuantidade.getText().toString());
        }
        // criar intent
        Intent it = new Intent(this,Pedido.class);
        it.putExtra("Tipo", tipo);
        it.putExtra("Sabor", sabor);
        it.putExtra("Qtde", qtde);
        startActivity(it);
    }

    public void checkButton(View v) {
        if (v.getId() == R.id.rgSabor) {
            int radioId = rgSabor.getCheckedRadioButtonId();
            RadioButton btRadio = findViewById(radioId);
            Toast.makeText(this, "Select radio button : " + btRadio.getText(), Toast.LENGTH_LONG).show();
        }  else{
            Toast.makeText(this, "Error in Select radio button : " , Toast.LENGTH_LONG).show();
        }
    }

    //Método para fechar o app obs. apartir da Api 21
    public void fechar(View v)
    {
        finishAndRemoveTask();
    }
}