package com.example.sorveteriasojos;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Pedido extends AppCompatActivity {
    Button btnVoltar;
    TextView txtPedido;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pedido);

        btnVoltar = findViewById(R.id.btnVoltar);
        txtPedido = findViewById(R.id.txtPedido);

        //recebe os dados da tela principal
        Intent it = getIntent();
        String tipo = it.getStringExtra("Tipo");
        String sabor = it.getStringExtra("Sabor");
        Integer qtde = it.getIntExtra("Qtde", 0);

        double valorTotal = 0;
        if (tipo.equals("Cone")) {
            valorTotal = qtde * 2.50;
        } else if (tipo.equals("Sunday")) {
            valorTotal = qtde * 4.50;
        } else if (tipo.equals("Picolé")) {
            valorTotal = qtde * 3.50;
        } else if (tipo.equals("Bola Dupla")) {
            valorTotal = qtde * 5.50;
        }

        //exibir resultado
        String sb = "Tipo: " + tipo + "\n" +
                "Sabor: " + sabor + "\n" +
                "Qtde: " + qtde + "\n" +
                "\n" + "\n" +
                "Valor Total: " + String.format("%.2f", valorTotal) + "\n";
        txtPedido.setText(sb);

    }
    public void onClick(View v){
        if (v.getId() == R.id.btnVoltar)
        {
            onBackPressed();
        }

    }
}